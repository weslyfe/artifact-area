# artifact-area

## Automated Rapid Artefact Surface Area Measurement from Imagery with Computer Vision
#### Authors: Wesley Weatherbee, Jonathan Fowler, Danika van Proosdij (2021)

This repository contains Supplementary Data accompanying the article <i>Automated Rapid Artefact Surface Area Measurement from Imagery with Computer Vision</i> submitted to the <i>Journal of Lithic Studies</i>.

## USAGE

Install required modules:


<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="SA_table_files/filelist.xml">
<style id="Book1_31207_Styles">
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
.font531207
	{color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.font631207
	{color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;}
.xl1531207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6331207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6431207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6531207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:9.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl6631207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6731207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.0pt solid #999999;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6831207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:italic;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6931207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid #767171;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7031207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.0pt solid #999999;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7131207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:1.5pt solid #767171;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7231207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.0pt solid #999999;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7331207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.5pt solid #767171;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7431207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.0pt solid #999999;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7531207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7631207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7731207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:1.0pt solid #999999;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl7831207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7931207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:none;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl8031207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8131207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:justify;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8231207
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:middle;
	border-top:none;
	border-right:1.0pt solid #999999;
	border-bottom:1.5pt solid #767171;
	border-left:1.0pt solid #999999;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
-->
</style>


<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Book1_31207" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=582 style='border-collapse:
 collapse;table-layout:fixed;width:437pt'>
 <col width=64 style='width:48pt'>
 <col width=212 style='mso-width-source:userset;mso-width-alt:7537;width:159pt'>
 <col width=178 style='mso-width-source:userset;mso-width-alt:6343;width:134pt'>
 <col width=64 span=2 style='width:48pt'>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6331207 width=64 style='height:15.0pt;width:48pt'><span
  style='mso-ansi-language:EN-CA'>Name</span></td>
  <td class=xl6431207 width=212 style='width:159pt'><span style='mso-ansi-language:
  EN-CA'>Summary</span></td>
  <td class=xl6431207 width=178 style='width:134pt'><span style='mso-ansi-language:
  EN-CA'>Reference</span></td>
  <td class=xl6531207 width=64 style='width:48pt'><span style='mso-ansi-language:
  EN-CA'>Installation</span></td>
  <td class=xl6531207 width=64 style='width:48pt'><span style='mso-ansi-language:
  EN-CA'>Alias</span></td>
 </tr>
 <tr height=52 style='height:39.0pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1258;mso-yfti-irow:0'>
  <td rowspan=2 height=72 class=xl6931207 style='border-bottom:1.0pt solid #999999;
  height:54.0pt;border-top:none'><span style='mso-ansi-language:EN-CA'>OpenCV</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source
  computer vision and machine learning library built to simplify and streamline
  adoption of machine learning and computer vision.</td>
  <td rowspan=2 class=xl7131207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Bradski 2000)</span></td>
  <td rowspan=2 class=xl7331207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  opencv-python==4.5.5.62</span></td>
  <td rowspan=2 class=xl7331207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>cv2</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Version 4.5.5.62</span></td>
 </tr>
 <tr height=38 style='height:28.8pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1241;mso-yfti-irow:1'>
  <td rowspan=2 height=58 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:43.8pt;border-top:none'><span style='mso-ansi-language:EN-CA'>SciPy</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source
  mathematical algorithm and convenience function library built on-top of
  NumPy.</td>
  <td rowspan=2 class=xl7631207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Virtanen <font class="font631207">et al</font><font
  class="font531207">. 2020; “Distance Computations (Scipy.Spatial.Distance) —
  SciPy v1.6.3 Reference Guide” n.d.)</font></span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  scipy==1.3.3</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>distance</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Version 1.3.3</span></td>
 </tr>
 <tr height=38 style='height:28.8pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1258;mso-yfti-irow:2'>
  <td rowspan=2 height=58 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:43.8pt;border-top:none'><span style='mso-ansi-language:EN-CA'>imutils</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source
  convenience function and visualization library built on OpenCV, matplotlib,
  and NumPy.</td>
  <td rowspan=2 class=xl7631207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Rosebrock [2015] 2021; “My Imutils Package: A
  Series of OpenCV Convenience Functions” 2015)</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  imutils==0.5.3</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>perspective;
  contours</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Version 0.5.3</span></td>
 </tr>
 <tr height=38 style='height:28.8pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1258;mso-yfti-irow:3'>
  <td rowspan=2 height=58 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:43.8pt;border-top:none'><span style='mso-ansi-language:EN-CA'>NumPy</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source
  scientific computing library fundamental to many Python workflows.</td>
  <td rowspan=2 class=xl7631207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Harris <font class="font631207">et al</font><font
  class="font531207">. 2020)</font></span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  numpy==1.17.4</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>np</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Version 1.17.4</span></td>
 </tr>
 <tr height=26 style='height:19.2pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1258;mso-yfti-irow:4'>
  <td rowspan=2 height=46 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:34.2pt;border-top:none'><span style='mso-ansi-language:EN-CA'>ArgParse</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Native Python
  library simplifying user input to scripts through CMD.<span
  style='mso-spacerun:yes'>  </span></td>
  <td rowspan=4 class=xl7631207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Van Rossum &amp; Drake 2009)</span></td>
  <td rowspan=4 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>n/a</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>argparse</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Python 3 Version 3.7.5</span></td>
 </tr>
 <tr height=38 style='height:28.8pt;mso-prop-change-author:"Wesley Weatherbee";
  mso-prop-change-time:20210524T1258;mso-yfti-irow:5'>
  <td rowspan=2 height=58 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:43.8pt;border-top:none'><span style='mso-ansi-language:EN-CA'>CSV</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Native Python
  library simplifying read and write of CSV files from multiple sources.</td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>csv</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'>Python 3 Version 3.7.5</span></td>
 </tr>
 <tr height=26 style='mso-height-source:userset;height:19.2pt;mso-prop-change-author:
  "Wesley Weatherbee";mso-prop-change-time:20210524T1258;mso-yfti-irow:6'>
  <td rowspan=2 height=46 class=xl7531207 style='border-bottom:1.0pt solid #999999;
  height:34.2pt;border-top:none'><span style='mso-ansi-language:EN-CA'>matplotlib</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source 2D
  graphics library for visualization and app development.</span></td>
  <td rowspan=2 class=xl7631207 style='border-bottom:1.0pt solid #999999;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(Hunter [2011] 2021; 2007)</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  matplotlib==3.1.2</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.0pt solid #999999;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>n/a</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6731207 style='height:15.0pt'><span style='mso-ansi-language:
  EN-CA'>Version 3.1.2</span></td>
 </tr>
 <tr height=77 style='height:57.6pt'>
  <td rowspan=2 height=97 class=xl7531207 style='border-bottom:1.5pt solid #767171;
  height:72.6pt;border-top:none'><span style='mso-ansi-language:EN-CA'>pandas</span></td>
  <td class=xl6631207><span style='mso-ansi-language:EN-CA'>Open-source data
  structure and statistical tools library designed to make scientific Python a
  more attractive and practical statistical computing environment for academic
  and industry practitioners alike (McKinney 2010, 56).<span
  style='mso-spacerun:yes'> </span></span></td>
  <td rowspan=2 class=xl7631207 style='border-bottom:1.5pt solid #767171;
  border-top:none'><span lang=EN-GB style='mso-bidi-font-family:Calibri;
  mso-bidi-font-size:12pt'>(McKinney 2010)</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.5pt solid #767171;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pip install
  pandas==0.25.3</span></td>
  <td rowspan=2 class=xl7731207 width=64 style='border-bottom:1.5pt solid #767171;
  border-top:none;width:48pt'><span style='mso-ansi-language:EN-CA'>pd</span></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6831207 style='height:15.0pt'><span style='mso-ansi-language:
  EN-CA'>Version 0.25.3</span></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=64 style='width:48pt'></td>
  <td width=212 style='width:159pt'></td>
  <td width=178 style='width:134pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>


<p>open command-line (cmd.exe) and change the directory to the directory where the file is loaded using: </p> <i>cd [path to directory here]</i> <br><br><p>after changing the directory, enter the following code in command-line: </p><i>python artifact-area.py --image images/test_01.jpg --width 2.388</i><br>

<p>Each contour iterated and measured will be appended to a newly made CSV table in the <i>csv</i> subdirectory located at <i>/artifact-area/csv</i>. Currently, this folder contains the raw experimental results from the article. </p> <br> <p>The <i>images</i> subdirectory located at <i>/artifact-area/images</i> contains input images to the script. Feel free to add your own. Currently the <i>images</i> subdirectory contains the image used in the article, names <i>test_01.jpg</i>.

## OUTPUT

<p>The script will display a window for each contour on the screen. Click or press any key with the window actively selected to prompt the next contour to display.</p>
<br><br>
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>
        <br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">This repository </span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
